import logging
import csv
import os
import os.path
header_file = ['Name', 'Address', 'Phone', 'Mail']


class Users_Directory():
    # Main Menu
    file_name = "DirectoryFile.csv"
    data_colum_name = []
    data_colum_address = []
    data_colum_phone = []
    data_colum_email = []
    user_information_list = []
    name = []
    address = []
    phone = []
    email = []

    logging.basicConfig(level=logging.DEBUG)
    def Menu(self):
        print('******WELCOME TO USER DIRECTORY******\n')
        print("1.- Creation of new record\n"
              "2.- Save all records in a file\n"
              "3.- Load records from a file\n"
              "4.- Search and get data from a given record}\n"
              "5.- Quit"
              )
    # Get Data from user
    def Select_Menu_Option(self):
        # Option to select from Menu
        menu_option_select = 0
        while True:
            menu_option_select = input('Select an option:')
            if menu_option_select.isdigit() != True:
                logging.error('Invalid option. Try again')
                break
            if menu_option_select == '1':
                # User information
                self.name = input('Give me your fullname:')
                if (' ' in self.name) == False:
                    logging.error('Name and Last Name is required. Try again')
                    break
                else:
                    split_name = self.name.rpartition(' ')
                    for split_name_counter in range(len(split_name)):
                        if ((split_name[split_name_counter].isalpha() == True) or (split_name[split_name_counter].isspace() == True)):
                            self.address = input('Give me an address:')
                            self.phone = input('Give me a phone:')
                            if len(self.phone) < 10:
                                logging.error('You need type at least 10 numbers')
                            self.email = input('give me a e-mail: ')
                            print('You are registering to', self.name, ' with address in ', self.address, 'number ', self.phone, 'and email ', self.email)
                            logging.info('Register success')
                            self.Menu()
                            self.Select_Menu_Option()
                        else:
                            logging.error('Numbers not allowed\n')
                            break
            # Save all records in a file
            elif menu_option_select == '2':
                if self.name == []:
                    logging.error('NO DATA TO SAVE')
                    break
                elif os.path.exists('DirectoryFile.csv') == False:
                    file_to_open = open(self.file_name, "a")
                    header_file_ = ','.join(header_file)
                    file_to_open.write(header_file_)
                    file_to_open.write('\n')
                    file_to_open.close()
                    file_to_open = open(self.file_name, "a")
                    user_information = [self.name, self.address,self.phone, self.email]
                    self.user_information_list.append(user_information)
                    #self.user_information_list_.append(self.user_information_list)
                    user_information_write = ','.join(user_information)
                    file_to_open.write(user_information_write)
                    file_to_open.write('\n')
                    file_to_open.close()
                elif os.path.exists('DirectoryFile.csv') == True:
                    file_to_open = open(self.file_name, "a")
                    user_information = [self.name, self.address, self.phone, self.email]
                    self.user_information_list.append(user_information)
                    #self.user_information_list_.append(self.user_information_list)
                    user_information_write = ','.join(user_information)
                    file_to_open.write(user_information_write)
                    file_to_open.write('\n')
                    file_to_open.close()
                    print('You are saved to', self.name, 'successfully')
                self.Menu()
            #Load records from a file
            elif menu_option_select == '3':
                with open(self.file_name, newline='') as File:
                    reader = csv.reader(File)
                    for colum in reader:
                        self.data_colum_name.append(colum[0])
                        self.data_colum_address.append(colum[1])
                        self.data_colum_phone.append(colum[2])
                        self.data_colum_email.append(colum[3])
                    if len(self.data_colum_name)<2:
                        logging.error('csv file empty, no data to load')
                        break
                    else:
                        print('----------------------------------------------------------------------------------------------------------')
                        for i in range(len(self.data_colum_name)):
                            print('{4}{0:20s}{4} {4}{1:30s}{4} {4}{2:15s}{4} {4}{3:30s}{4}'.format(self.data_colum_name[i], self.data_colum_address[i], self.data_colum_phone[i],self.data_colum_email[i],'|'))
                        print('----------------------------------------------------------------------------------------------------------')
                self.Menu()
                print('')
            # Get information
            elif menu_option_select == '4':
                print('')
                print('******Search and get data from a given record******\n')
                print('1.- Search by Name ')
                print('2.- Search by Address ')
                print('3.- Search by Phone ')
                print('4.- Search by Email ')
                print('5.- Exit')

                data_given = input('Select an option:')
                while True:
                    with open(self.file_name, newline='') as File:
                        reader = csv.reader(File)
                        for colum in reader:
                            self.data_colum_name.append(colum[0])
                            self.data_colum_address.append(colum[1])
                            self.data_colum_phone.append(colum[2])
                            self.data_colum_email.append(colum[3])
                        if data_given == '1':
                            name_to_search = input('Give me a name to search: ')
                            if (name_to_search in self.data_colum_name) == False:
                                logging.error('Name not founded. Try again')
                                break
                            for name_counter in range (len(self.data_colum_name)):
                                if name_to_search in self.data_colum_name[name_counter]:
                                    print('Name:', self.data_colum_name[name_counter])
                                    print('Address:', self.data_colum_address[name_counter])
                                    print('Phone:', self.data_colum_phone[name_counter])
                                    print('Email:', self.data_colum_email[name_counter])
                        if data_given == '2':
                            address_to_search = input('Give me an address to search: ')
                            if (address_to_search in self.data_colum_address) == False:
                                logging.error('Address not founded. Try again')
                                break
                            for address_counter in range (len(self.data_colum_address)):
                                if address_to_search in self.data_colum_address[address_counter]:
                                    print('Name:', self.data_colum_name[address_counter])
                                    print('Address:', self.data_colum_address[address_counter])
                                    print('Phone:', self.data_colum_phone[address_counter])
                                    print('Email:', self.data_colum_email[address_counter])
                        if data_given == '3':
                            phone_to_search = input('Give me a phone to search: ')
                            if (phone_to_search in self.data_colum_phone) == False:
                                logging.error('Phone not founded. Try again')
                                break
                            for phone_counter in range (len(self.data_colum_phone)):
                                if phone_to_search in self.data_colum_phone[phone_counter]:
                                    print('Name:', self.data_colum_name[phone_counter])
                                    print('Address:', self.data_colum_address[phone_counter])
                                    print('Phone:', self.data_colum_phone[phone_counter])
                                    print('Email:', self.data_colum_email[phone_counter])
                        if data_given == '4':
                            email_to_search = input('Give me a phone to search: ')
                            if (email_to_search in self.data_colum_email) == False:
                                logging.error('Phone not founded. Try again')
                                break
                            for email_counter in range (len(self.data_colum_email)):
                                if email_to_search in self.data_colum_email[email_counter]:
                                    print('Name:', self.data_colum_name[email_counter])
                                    print('Address:', self.data_colum_address[email_counter])
                                    print('Phone:', self.data_colum_phone[email_counter])
                                    print('Email:', self.data_colum_email[email_counter])
                        if menu_option_select == '5':
                            break
            else:
                logging.error('Option not valid. Try again')
                break

# Main
Users_Directory_ = Users_Directory()
Users_Directory_.Menu()
Users_Directory_.Select_Menu_Option()

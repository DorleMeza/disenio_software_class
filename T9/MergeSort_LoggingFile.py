import csv
import logging
# Merge sort 
#Merge sort is an efficient, general-purpose, comparison-based sorting algorithm. 
#Most implementations produce a stable sort, which means that the order of equal elements is the same in the input and output. 
#Merge sort is a divide and conquer algorithm that was invented by John von Neumann in 1945
class SortClass():

    data_from_file = []
    ##This method collect data from a CSV file and check if the data is alphanumeric
    def set_input_data(self, file_path_name):

        if file_path_name.find('.csv') > 0:  # Is the file .csv?
            with open(file_path_name) as file_path_name_:
                file_path_name_read = csv.reader(file_path_name_,delimiter=',',quotechar=',',quoting=csv.QUOTE_MINIMAL)
                for filas in file_path_name_read:
                    for columnas in range(len(filas)):
                        if filas[columnas].isalnum() == True:  # Are the inputs alphanumeric?
                            self.data_from_file.append(filas[columnas])
                        else:
                            logging.error('You must use alphanumeric symbols')

        else:
            logging.error('The file does not have a CSV format')
        return self.data_from_file
        # A merge sort works as follows:
        #     * Divide the unsorted list into n sublists, each containing one element (a list of one element is considered sorted).
        #     * Repeatedly merge sublists to produce new sorted sublists until there is only one sublist remaining. This will be the sorted list.
    def execute_merge_sort(self, data_to_sort):
        if len(data_to_sort) > 1:
            medium = len(data_to_sort) // 2
            left_side = data_to_sort[:medium]
            right_side = data_to_sort[medium:]
            self.execute_merge_sort(left_side)
            self.execute_merge_sort(right_side)
            index_left = 0
            index_right = 0
            index_sort = 0
            while index_left < len(left_side) and index_right < len(right_side):
                if left_side[index_left] <= right_side[index_right]:
                    data_to_sort[index_sort] = left_side[index_left]
                    index_left = index_left + 1
                else:
                    data_to_sort[index_sort] = right_side[index_right]
                    index_right = index_right + 1
                index_sort = index_sort + 1

            while index_left < len(left_side):
                data_to_sort[index_sort] = left_side[index_left]
                index_left = index_left + 1
                index_sort = index_sort + 1

            while index_right < len(right_side):
                data_to_sort[index_sort] = right_side[index_right]
                index_right = index_right + 1
                index_sort = index_sort + 1
        return data_to_sort
    ##This method save the data sorted in a CSV file 
    def set_output_data(self):

        data_to_write_append = []
        file_to_save= 'FileSorted_Merge.csv'
        data_to_write = self.execute_merge_sort(data_to_sort)
        file_to_open = open(file_to_save, "a")
        for data_to_write_ in range(len(data_to_write)):
            data_to_write_append.append(data_to_write[data_to_write_])
        information_to_write = ','.join(data_to_write_append)
        file_to_open.write(information_to_write)
        file_to_open.write('\n')
        file_to_open.close()
        logging.info('file saved successfully')


#Main
file_path_name = input('Give me the file to sort:')
SortClass_ = SortClass()
data_to_sort = SortClass_.set_input_data(file_path_name)
SortClass_.execute_merge_sort(data_to_sort)
SortClass_.set_output_data()

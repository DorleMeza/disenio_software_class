import csv
from tabulate import tabulate
header_file = ['Name', 'Address', 'Phone', 'Mail']
class Users_Directory():
    #Main Menu
    file_name = "DirectoryFile.csv"
    user_information_list = []
    def Menu(self):
        print('******WELCOME TO USER DIRECTORY******')
        print('')
        print('1.- Creation of new record')
        print('2.- Save all records in a file')
        print('3.- Load records from a file')
        print('4.- Search and get data from a given record')
        print('5.- Quit')
    #Create a CSV File
    def create_csv_file(self):
        file_to_open = open(self.file_name, "a")
        header_file_ = ','.join(header_file)
        file_to_open.write(header_file_)
        file_to_open.write('\n')
        file_to_open.close()

    # Get Data from user
    def Select_Menu_Option(self):
        while True :
            #Option to select from Menu
            menu_option_select= input('Select an option:')
            if menu_option_select == '1':
                #User information
                name = input('Give me a name:')
                address = input('Give me an address:')
                phone = input('Give me a phone:')
                email = input('give me a e-mail: ')
            #Save all records in a file
            elif menu_option_select == '2':

                file_to_open = open(self.file_name, "a")
                user_information = [name,address,phone,email]
                self.user_information_list.append(user_information)
                user_information_write = ','.join(user_information)
                file_to_open.write(user_information_write)
                file_to_open.write('\n')
                file_to_open.close()
                print('Records in a file saved successfully')

            #Load records from a file
            elif menu_option_select =='3':
                with open(self.file_name, newline='') as File:
                    reader = csv.reader(File)
                    for row in reader:
                        print(row)

            elif menu_option_select == '4':
                print('1.- Search and get data from name: ')
                print('2.- Search and get data from address: ')
                print('3.- Search and get data from a phone: ')
                print('4.- Search and get data from a email: ')
                option_select = input('Select an option_: ')
                if option_select == '1':
                    option1 = input ('Give me the name to search:')
                    for users in range (len(self.user_information_list)):
                        list_users = self.user_information_list[users]
                        if list_users[users] == option1:
                            print(self.user_information_list[users])


            elif menu_option_select == '5':
                break
            else:
                print('Invalid Option, try again')

#Main
Users_Directory_=Users_Directory()
Users_Directory_.Menu()
Users_Directory_.create_csv_file()
Users_Directory_.Select_Menu_Option()


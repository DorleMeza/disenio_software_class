import unittest
from Factorial import Factorial

class MyTestCase(unittest.TestCase):
    def test_factorial(self): #function working correctly
        self.assertEqual(Factorial(5), 120)

    def test_factorial_negative_numbers(self): #Negative numbers
        with self.assertRaises(ValueError):
            Factorial(-1)

    def test_factorial_zero(self): #0 factorial
        with self.assertRaises(ValueError):
            Factorial(0)

if __name__ == '__main__':
    unittest.main()


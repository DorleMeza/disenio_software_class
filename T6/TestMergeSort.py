import unittest
from MergeSort import SortClass

class TestMergeSort(unittest.TestCase):
    #This method test QuickSortClass using positive numbers
    def test_sort_with_positive_numbers(self):
        SortClass_ = SortClass()
        self.assertEqual(SortClass_.execute_merge_sort([5, 5, 7, 8, 2, 4, 1]),[1, 2, 4, 5, 5, 7, 8])
    #This method test QuickSortClass using negative numbers
    def test_sort_with_negative_numbers(self):
        SortClass_ = SortClass()
        self.assertEqual(SortClass_.execute_merge_sort([-5, -5, -7, -8, -2, -4, -1]), [-8, -7, -5, -5, -4, -2, -1])
    #This method test QuickSortClass using empty list
    def test_sort_with_empty_list(self):
        SortClass_ = SortClass()
        self.assertEqual(SortClass_.execute_merge_sort([]), [])
    #This method test QuickSortClass using letters
    def test_sort_with_letters(self):
        SortClass_ = SortClass()
        self.assertEqual(SortClass_.execute_merge_sort(['a','Y','X','t','c']), ['X', 'Y', 'a', 'c', 't'])
    #This method test QuickSortClass using unsorted tuples
    def test_merge_sort_on_unsorted_tuples(self):
        SortClass_ = SortClass()
        self.assertIsNotSortClass_.execute_merge_sort([(5, 'B'), (3, 'A')])

if __name__ == '__main__':
    unittest.main()

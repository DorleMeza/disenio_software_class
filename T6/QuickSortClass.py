import csv
#Quicksort Algorothm
#It is an efficient sorting algorithm. Developed by British computer scientist Tony Hoare in 1959[1] and published in 1961
class QuickSortClass():
    data_from_file = []
    ##This method collect data from a CSV file and check if the data is alphanumeric
    def set_input_data(self, file_path_name):
        ##Are you given a CSV file?
        if file_path_name.find('.csv') > 0: }
            with open(file_path_name) as file_path_name_:
                file_path_name_read = csv.reader(file_path_name_,delimiter=',',quotechar=',',quoting=csv.QUOTE_MINIMAL)
                for filas in file_path_name_read:
                    for columnas in range(len(filas)):
                        ##Is the data alphanumeric_
                        if filas[columnas].isalnum() == True:  
                            self.data_from_file.append(filas[columnas])
                        else:
                            print('ERROR: You must use alphanumeric symbols')
        else:
            print('ERROR:The file does not have a CSV format')
        return self.data_from_file
    ##This method do a quick sort
    # The basic version of the algorithm does the following:
    #     * Divide the collection in two (roughly) equal parts by taking a pseudo-random element and using it as a pivot.
    #     * Elements smaller than the pivot get moved to the left of the pivot, and elements larger than the pivot to the right of it.
    #     * This process is repeated for the collection to the left of the pivot, as well as for the array of elements to the right of the pivot until the whole array is sorted.
    # When we describe elements as "larger" or "smaller" than another element - it doesn't necessarily mean larger or smaller integers, we can sort by any property we choose.
    # If we have a custom class Person, and each person has a name and age, we can sort by name (lexicographically) or by age (ascending or descending).
    def execute_quick_sort(self, data_to_sort):
        if len(data_to_sort) < 2:
            return data_to_sort
        else:
            pivot = data_to_sort[0]
            less = [i for i in data_to_sort[1:] if i <= pivot]
            greater = [i for i in data_to_sort[1:] if i > pivot]
            return self.execute_quick_sort(less) + [pivot] + self.execute_quick_sort(greater)
    ##This method save the data sorted in a CSV file 
    def set_output_data(self):
        data_to_write_append = []
        file_to_save= 'FileSorted_Quick.csv'
        data_to_write = self.execute_quick_sort(data_to_sort)
        file_to_open = open(file_to_save, "a")
        for data_to_write_ in range(len(data_to_write)):
            data_to_write_append.append(data_to_write[data_to_write_])
        information_to_write = ','.join(data_to_write_append)
        file_to_open.write(information_to_write)
        file_to_open.write('\n')
        file_to_open.close()
        print('file saved successfully')

#main


file_path_name = input('Give me the file to sort:')
quicksortclass_ = QuickSortClass()
data_to_sort = quicksortclass_.set_input_data(file_path_name)
quicksortclass_.execute_quick_sort(data_to_sort)
quicksortclass_.set_output_data()
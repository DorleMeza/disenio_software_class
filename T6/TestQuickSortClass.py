import unittest
from QuickSortClass import QuickSortClass

#Unittest Quick sort
class TestQuickSort(unittest.TestCase):
    #This method test QuickSortClass using positive numbers
    def test_sort_with_positive_numbers(self):
        QuickSortClass_ = QuickSortClass()
        self.assertEqual(QuickSortClass_.execute_quick_sort([5, 5, 7, 8, 2, 4, 1]),[1, 2, 4, 5, 5, 7, 8])
    #This method test QuickSortClass using negative numbers
    def test_sort_with_negative_numbers(self):
        QuickSortClass_ = QuickSortClass()
        self.assertEqual(QuickSortClass_.execute_quick_sort([-5, -5, -7, -8, -2, -4, -1]), [-8, -7, -5, -5, -4, -2, -1])
    #This method test QuickSortClass using empty list
    def test_sort_with_empty_list(self):
        QuickSortClass_ = QuickSortClass()
        self.assertEqual(QuickSortClass_.execute_quick_sort([]), [])
    #This method test QuickSortClass using letters
    def test_sort_with_letters(self):
        QuickSortClass_ = QuickSortClass()
        self.assertEqual(QuickSortClass_.execute_quick_sort(['a','Y','X','t','c']), ['X', 'Y', 'a', 'c', 't'])


if __name__ == '__main__':
    unittest.main()

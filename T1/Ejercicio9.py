# Write a function that converts a decimal number into a Roman format

def RomanFormat():
    #CSV Library
    import csv

    # Units Dictionary
    U = {
        0: '',
        1: 'I',
        2: 'II',
        3: 'III',
        4: 'IV',
        5: 'V',
        6: 'VI',
        7: 'VII',
        8: 'VIII',
        9: 'IX'
    }
    # Tens Dictionary
    D = {
        0: '',
        10: 'X',
        20: 'XX',
        30: 'XXX',
        40: 'XL',
        50: 'L',
        60: 'LX',
        70: 'LXX',
        80: 'LXXX',
        90: 'XC'
    }
    # Hundreds Dictionary
    C = {
        0: '',
        100: 'C',
        200: 'CC',
        300: 'CCC',
        400: 'CD',
        500: 'D',
        600: 'DC',
        700: 'DCC',
        800: 'DCCC',
        900: 'CM'
    }
    # Thousands Dictionary
    M = {
        1000: 'M',
        2000: 'MM',
        3000: 'MMM'
    }

    with open('DataSet2.csv', newline='') as File:
        reader = csv.reader(File)
        for row in reader:
            for dec_num in row:
                # The Maximun Number is 3999
                if (int(dec_num) > 0) and (int(dec_num) <= 3999):
                    # Units to be separated
                    units = (int(dec_num) % 10)
                    tens = (int(dec_num) % 100) - units
                    hundreds = (int(dec_num) % 1000) - tens - units
                    thousands = (int(dec_num) % 10000) - hundreds - tens - units

                    # Dec to Bin - Shape the dictionary
                    str_thousands = M.get(thousands)
                    str_hundreds = C.get(hundreds)
                    str_tens = D.get(tens)
                    str_units = U.get(units)

                    # Avoid 'None" string
                    if str_thousands is None:
                        Dec_to_Rom = str(str_hundreds) + str(str_tens) + str(str_units)
                    if str_hundreds is None:
                        Dec_to_Rom = str(str_tens) + str(str_units)
                    if str_tens is None:
                        Dec_to_Rom = str(str_units)
                    if str_thousands is not None:
                        Dec_to_Rom = str(str_thousands) + str(str_hundreds) + str(str_tens) + str(str_units)

                    # Print Result
                    print('Decimal number',dec_num,'is in Roman Format:',Dec_to_Rom)

                else:
                    Dec_to_Rom = 'ERROR'
                    print('ERROR : Max Decimal Number is 3999')






#Write a module containing different function that computes the
#   1. Sample mean
#   2. Sample standard deviation
#   3. Median
#   4. A function that returns the n-quartil
#   5. A function that returns the n-percentil

import math
input_string = [164,168,168,168,170,170,170,171,171,174,177,178,178,178,179,180,180,183,186,186,188,190,192,195]

print('Data Input:',input_string)
#Calculate Sample Mean
def Sample_Mean():
    sum_samples = 0
    for n_samples in range(len(input_string)):
        sum_samples = sum_samples + input_string[n_samples]
    sample_mean_result = sum_samples / (n_samples+1)
    print ('Sample mean from is: ',sample_mean_result)
    return(sample_mean_result)

def Standard_Deviation():
    Subtract_Mean = 0
    Sum_Mean = 0
    Square_add = 0
    mean = Sample_Mean()
    for n_samples in range(len(input_string)):
        Subtract_Mean= input_string[n_samples] - mean
        Square = (Subtract_Mean)*(Subtract_Mean)    #Square each of the differences from the previous step and make a list of the squares
        Square_add =  Square_add + Square           #Add the squares from the previous step together
    div = (len(input_string)) - 1
    stan_dev_div = Square_add / div
    stan_dev_result =  math.sqrt(stan_dev_div)      #Take the square root of the number.
    print ('Standard Deviation is: ',stan_dev_result)
    return(stan_dev_result)

def Median ():
    count=0
    sort_data = sorted (input_string)
    if ((len(input_string))%2) == 0:
        for count_data in range (len(input_string)):
            count=count+1
        middle1 = (count/2)
        median_result1 = sort_data[(int(middle1)) - 1]
        median_result2 = sort_data[int(middle1)]
        median_result = (median_result1 + median_result2 )/2
        print('Median is: ', median_result)
        return (median_result)
    else:
        for count_data in range (len(input_string)):
            count=count+1
        middle = (count/2) + 0.5
        median_result = sort_data [(int(middle))-1]
        print('Median is: ', median_result)
        return (median_result)

def n_quartil():
    count_data=0
    sort_data = sorted (input_string)
    for count_data in range (len(input_string)):
        count_data = count_data +1
    Q3 = 3/4*(count_data + 1)
    if (Q3%2) ==0:
        Upper_Quartile = sort_data[(int(Q3)) - 1]
        print('Upper_Quartile:',Upper_Quartile)
    else:
        Upper_Quartile1 = sort_data[(int(Q3)) - 1]
        Upper_Quartile2 = sort_data[(int(Q3))]
        Upper_Quartile = (Upper_Quartile1+Upper_Quartile2)/2
        print('Upper_Quartile:',Upper_Quartile)
    Q2 = 2/4*(count_data + 1)
    if (Q2%2) ==0:
        Middle_Quartile = sort_data[(int(Q2)) - 1]
        print('Middle_Quartile',Middle_Quartile)
    else:
        Middle_Quartile1 = sort_data[(int(Q2)) - 1]
        Middle_Quartile2 = sort_data[(int(Q2))]
        Middle_Quartile = (Middle_Quartile1+Middle_Quartile2)/2
        print('Middle_Quartile',Middle_Quartile)
    Q1 = 1/4*(count_data + 1)
    if (Q1%2) ==0:
        Lower_Quartile = sort_data[(int(Q1)) - 1]
        print('Lower_Quartile',Lower_Quartile)
    else:
        Lower_Quartile1 = sort_data[(int(Q1)) - 1]
        Lower_Quartile2 = sort_data[(int(Q1))]
        Lower_Quartile = (Lower_Quartile1+Lower_Quartile2)/2
        print('Lower_Quartile',Lower_Quartile)

def n_percentil():
    count_data = 0
    k = 75/100  #percentil
    sort_data = sorted (input_string)
    for count_data in range(len(input_string)):
        count_data = count_data +1
    i = count_data * k
    if (i%2) ==0:
        P = (i+(i+1))/2
        percentil = (sort_data[(int(P-1))] + sort_data[(int(P))])/2
        print ('percentil',percentil)
    else:
        P = i+1
        percentil = sort_data[(int(P))]
        print ('percentil',percentil)
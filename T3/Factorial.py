import math
#Calculate the factorial of a number
def Factorial(num):
    if (num != 0) and (num > 0):
        factorial = math.factorial(num)
        return(factorial)
    else:
        raise ValueError('Give me another value!')